PRAGMA foreign_keys= OFF;
BEGIN TRANSACTION;
create table rapports
(
    id_rapport  char(80)
        constraint rapports_pk
            primary key,
    expediteur  char(80),
    description text,
    id_odm      char(80)
);

create table mission
(
    id_mission char(80)
        constraint mission_pk
            primary key,
    descriptif text,
    id_rapport char(80)
        constraint mission_rapports_id_rapport_fk
            references rapports,
    ODMs       text,
    status     integer
);

create table user
(
    id_user       char(80)
        constraint user_pk
            primary key,
    equipe        char(80),
    adressepilote char(80),
    adresserover  char(80)
);

create table odm
(
    id_odm       char(80)
        constraint odm_pk
            primary key,
    instructions text,
    id_user      char(80)
        constraint odm_user_id_user_fk
            references user,
    id_rapport   char(80)
        constraint odm_rapports_id_rapport_fk
            references rapports,
    id_mission   char(80)
        constraint odm_mission_idMission_fk
            references mission
);

insert into mission (id_mission, descriptif, id_rapport)
values ('MISSION_2023-01-01T00:00:00Z', 'Demo01', '');

insert into mission (id_mission, descriptif, id_rapport)
values ('MISSION_2023-01-02T00:00:00Z', 'Demo02', '');

insert into odm (id_odm, instructions)
values ('ODM_2023-01-01T00:00:00Z', 'Demo01');

insert into rapports (id_rapport, expediteur, description, id_odm)
values ('RDM_2023-01-01T00:02:00Z', 'Exp01', 'RDM Demo01', 'ODM_2023-01-01T00:00:00Z');

COMMIT;

