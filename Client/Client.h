#ifndef PRJAKKODIS_CLIENT_H
#define PRJAKKODIS_CLIENT_H

#include <iostream>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>

using namespace std;

class Client {
public:
    Client(string adresse_ip, int port);

    ~Client();

    void ouvrirConnexion();

    void fermerConnexion();

    void envoyerData(string data);

    string getIpAddr() { return ipAddr_; }

    int getPort() { return port_; }

    int getSocket() { return socket_; }

    void setIpAddr(string ipAddr) { ipAddr_ = ipAddr; }

    void setPort(int port) { port_ = port; }

private:
    string ipAddr_;
    int port_;
    int socket_;
};

#endif //PRJAKKODIS_CLIENT_H
