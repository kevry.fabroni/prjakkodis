#ifndef PRJAKKODIS_MENU_MISSION_H
#define PRJAKKODIS_MENU_MISSION_H

#include "../Classes/Mission.h"
#include <iostream>
#include <list>
#include <cstring>
#include <string>

class menu_mission {
public:
    int start_creation();

    int start_consultation();
};


#endif //PRJAKKODIS_MENU_MISSION_H
