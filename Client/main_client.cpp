#include <iostream>
#include <sqlite3.h>

#include "Client.h"
#include "../Classes/ODM.h"
#include "menu_odm.h"
#include "menu_mission.h"

using namespace std;

int main() {

    int choix;

    cout << "Voulez-vous :" << endl;
    cout << "1. Créer une mission" << endl;
    cout << "2. Créer un ordre de mission" << endl;
    cout << "3. Consulter les missions" << endl;
    cout << "4. Consulter les ordres de missions" << endl;
    cin >> choix;
    cin.ignore();
    switch (choix) {
        case 1: //Création mission
            menu_mission creationMission;
            creationMission.start_creation();
            break;
        case 2: //Création ODM
            menu_odm menuOdm;
            menuOdm.start_creation();
            break;
        case 3: //Consultation mission
            menu_mission consulationMission;
            consulationMission.start_consultation();
            break;
        case 4://Consultation ODM
            menu_odm consultation_odm;
            consultation_odm.start_consultation();
            break;
        default:
            cout << "Choix invalide. Fin du programme" << endl;
    }
    cout << "Fin du programme." << endl;
    return 0;
}