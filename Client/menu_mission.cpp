#include "menu_mission.h"

using namespace std;

int menu_mission::start_creation() {
    cout << "Rédaction mission" << endl;
    string descriptifMission;
    cout << "Entrez le descriptif de la mission : " << endl;
    //cin.ignore();
    getline(cin, descriptifMission);
    Mission mission(descriptifMission);
    cout << "[" << mission.getIdMission() << "] Créée" << endl;

    sqlite3 *db;
    int rc;

    rc = sqlite3_open("../db/deminage.db", &db);
    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur ouverture base: " << sqlite3_errmsg(db) << endl;
        return 1;
    }
    cout << "[BDD] OK Ouverture de la base" << endl;

    mission.insert(db);

    sqlite3_close(db);
    cout << "Menu principal" << endl;
    return 0;
}

int menu_mission::start_consultation() {
    sqlite3 *db;
    int rc;
    rc = sqlite3_open("../db/deminage.db", &db);
    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur ouverture base: " << sqlite3_errmsg(db) << endl;
        return 1;
    }
    cout << "[BDD] OK Ouverture de la base" << endl;

    const char *rq = "SELECT id_mission, descriptif FROM mission";

    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(db, rq, strlen(rq), &stmt, nullptr);

    while (sqlite3_step(stmt) == SQLITE_ROW) {
        string id = (const char *) sqlite3_column_text(stmt, 0);
        string desc = (const char *) sqlite3_column_text(stmt, 1);
        Mission m(id, desc);
        m.afficher();
    }

    return 0;
}