#ifndef PRJAKKODIS_MENU_ODM_H
#define PRJAKKODIS_MENU_ODM_H

#include "Client.h"
#include "../Classes/ODM.h"

class menu_odm {
public:
    int start_creation();

    int start_consultation();
};


#endif //PRJAKKODIS_MENU_ODM_H
