/*!
 * \file Client.cpp
 * \brief Classe pour l'envoie de données aux pilotes
 * \author SeptimusTech
 */

#include "Client.h"

using namespace std;

Client::Client(string adresse_ip, int port) {
    setIpAddr(adresse_ip);
    setPort(port);
//    ouvrirConnexion();
}

Client::~Client() {
    if (getSocket() != 0) {
        this->fermerConnexion();
    }
}

void Client::ouvrirConnexion() {
    // TODO : vérifier affectation du port et de l'adresse dans l'objet appelant
    int sock;
    struct sockaddr_in serveur_addr;

    memset(&serveur_addr, 0, sizeof(serveur_addr));
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        cout << "[ERR] CREATION SOCKET IMPOSSIBLE" << endl;
    }

    // affectation addresse et port
    serveur_addr.sin_family = AF_INET;
    serveur_addr.sin_port = htons(this->getPort());

    //
    if (inet_pton(AF_INET, this->getIpAddr().c_str(), &serveur_addr.sin_addr) == -1) {
        cout << "[ERR] ADRESSE IP INVALIDE / NON SUPPORTEE" << endl;
    }

    //connexion
    if (connect(sock, (struct sockaddr*) &serveur_addr, sizeof(serveur_addr)) == -1) {
        cout << "[ERR] CONNEXION IMPOSSIBLE AVEC " << this->getIpAddr() << ":" << this->getPort() << endl;
    }
    this->socket_ = sock;
}

void Client::fermerConnexion() {
    close(this->socket_);
    this->socket_ = 0;
}

void Client::envoyerData(string data) {
    if (getSocket() == 0) {
        ouvrirConnexion();
    }
    if (send(getSocket(), data.c_str(), data.size(), 0) > 0) {
        cout << "[TRANSMIS] " << data << endl;
    } else {
        cout << "Echec transmission" << endl;
    }
}

