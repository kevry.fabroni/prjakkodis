#include "menu_odm.h"

using namespace std;

int menu_odm::start_creation() {

    int choixGroupe;
    cout << "Rédaction ordre de mission :" << endl;
    cout << "Choisissez le groupe auquel vous souhaitez envoyer des donnees : " << endl;
    cout << "1. Pilotes déminage" << endl;
    cout << "2. Pilotes observateur" << endl;
    cout << "3. Démonstration" << endl;
    cout << "Votre choix : ";
    cin >> choixGroupe;

    Client serveurGR1("57.128.105.127", 8080);
    Client serveurGR2("57.128.110.200", 8080);
    Client serveurDEMO("127.0.0.1", 8080);


    string missionInstruction;
    cout << "Entrez les instructions de la mission : ";
    cin.ignore(); // Ignore the newline character left in the buffer from the previous input.
    getline(cin, missionInstruction);

    ODM mission(missionInstruction);

    switch (choixGroupe) {
        case 1:
            serveurGR1.ouvrirConnexion();
            serveurGR1.envoyerData(mission.toJSON());
            serveurGR1.fermerConnexion();
            cout << "Ordre de mission envoyé aux démineurs." << endl;
            break;
        case 2:
            serveurGR2.ouvrirConnexion();
            serveurGR2.envoyerData(mission.toJSON());
            serveurGR2.fermerConnexion();
            cout << "Ordre de mission envoyé aux observateurs." << endl;
            break;
        case 3:
            serveurDEMO.ouvrirConnexion();
            serveurDEMO.envoyerData(mission.toJSON());
            serveurDEMO.fermerConnexion();
            cout << "Ordre de mission envoyé pour démonstration." << endl;
            break;
        default:
            cout << "Choix invalide. Le programme va se terminer." << endl;
            return 1;
    }
    sqlite3 *db;
    int rc;

    rc = sqlite3_open("../db/deminage.db", &db);
    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur ouverture base: " << sqlite3_errmsg(db) << endl;
        return 1;
    }
    cout << "[BDD] OK Ouverture de la base" << endl;

    mission.insert(db);

    sqlite3_close(db);
    cout << "Menu principal :" << endl;
    return 0;
}


int menu_odm::start_consultation() {
    sqlite3 *db;
    int rc;
    rc = sqlite3_open("../db/deminage.db", &db);
    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur ouverture base: " << sqlite3_errmsg(db) << endl;
        return 1;
    }
    cout << "[BDD] OK Ouverture de la base" << endl;

    const char *rq = "SELECT id_odm, instructions FROM odm";

    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(db, rq, strlen(rq), &stmt, nullptr);

    while (sqlite3_step(stmt) == SQLITE_ROW) {
        string id = (const char *) sqlite3_column_text(stmt, 0);
        string desc = (const char *) sqlite3_column_text(stmt, 1);
        ODM m(id, desc);
        m.afficher();
    }

    return 0;
}