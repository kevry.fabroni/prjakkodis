all:
	make -C ./Client all
	make -C ./Serveur all

clean:
	make -C ./Client clean
	make -C ./Serveur clean

mrproper:
	make -C ./Client mrproper
	make -C ./Serveur mrproper
