/*!
 * \file Server.h
 * \brief Classe du serveur
 * \author SeptimusTech
 */

#ifndef PRJAKKODIS_SERVER_H
#define PRJAKKODIS_SERVER_H

#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <iomanip>

#include "../Classes/Evenement.h"

using namespace std;
/*!
 * \class Server Server.h Server.cpp
 * \brief Classe du serveur d'écoute, utilisé pour l'enregistrement des données des pilotes
 */
class Server {
public:
    /*!
     * \brief Constructeur portant l'adresse et le port d'écoute
     * @param ipAddr Adresse d'écoute
     * @param port port d'écoute
     */
    Server(string ipAddr, int port);

    /*!
     * \brief Démarrage du service
     */
    void start();

    /*!
     * \brief Manipulation des connexions sur des threads
     * @param sock_fd Socket de connexion
     */
    static void* connection_handler(void* sock_fd);

private:
    string ipAddr_;
    int port_;
    int socket_;
    static int connection_;
    static int const concurrent_connection = 10;
};


#endif //PRJAKKODIS_SERVER_H
