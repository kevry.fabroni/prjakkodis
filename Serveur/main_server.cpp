#include "Server.h"

int main() {

    string ipLocal = "127.0.0.1";
    string ipDist = "57.128.107.86";

    int choixServeur;
    cout << "Choisissez le serveur ecoutant : " << endl;
    cout << "1. Local" << endl;
    cout << "2. Web" << endl;
    cout << "Votre choix : ";
    cin >> choixServeur;

    Server local(ipLocal, 8080);
    Server web(ipDist, 8080);

    switch (choixServeur) {
        case 1:
            local.start();
            break;
        case 2:
            web.start();
            break;
        default:
            cout << "Choix invalide, fin du programme." << endl;
            return 1;
    }
    return 0;
}