#include "Server.h"
// Thread stack size 64KB
#define THREAD_STACK_SIZE 65536
#define BUFFER_SIZE 1024

using namespace std;

int Server::connection_ = 0;

Server::Server(string ipAddr, int port) {
    ipAddr_ = ipAddr;
    port_ = port;
}

void Server::start() {
    pthread_t thread_id;
    pthread_attr_t attr;

    if (pthread_attr_init(&attr) != 0) {
        cout << "[ERROR][THREAD][INIT] " << strerror(errno) << endl;
    }

    if (pthread_attr_setstacksize(&attr, THREAD_STACK_SIZE) != 0) {
        cout << "[ERROR][THREAD][STACK] " << strerror(errno) << endl;
    }

    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0) {
        cout << "[ERROR][THREAD][DETACH] " << strerror(errno) << endl;
    }

    int master_socket, conn_id;
    struct sockaddr_in server, client;

    memset(&server, 0, sizeof(server));
    memset(&client, 0, sizeof(client));

    signal(SIGPIPE, SIG_IGN);


    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        cout << "[ERROR] CAN'T CREATE TO SOCKET" << endl;
    } else {
        cout << "[NOTE] SOCKET CREATED DONE" << endl;
    }

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(this->ipAddr_.c_str());
    server.sin_port = htons(this->port_);

    socklen_t addrlen = sizeof(struct sockaddr_in);

    if (bind(master_socket, (struct sockaddr*) &server, sizeof(server)) == -1) {
        cout << "[ERROR][BIND] " << strerror(errno) << endl;
    } else {
        cout << "[NOTE] BIND " << this->ipAddr_ << ":" << this->port_ << endl;
    }
    // Listen on the socket, with 20 max connection requests queued
    if (listen(master_socket, 20) == -1) {
        std::cout << "[ERROR][LISTEN] " << strerror(errno) << "\n";
        // return -1;
    } else {
        std::cout << "[INFO] WAITING FOR INCOMING CONNECTIONS\n";
    }

    while (true) {
        conn_id = accept(master_socket, (struct sockaddr*) &client, (socklen_t * ) & addrlen);

        if (conn_id == -1) {
            cout << "[WARNING] CAN'T ACCEPT NEW CONNECTION\n";
        } else {
            if (this->connection_ >= this->concurrent_connection) {
                cout << "[WARNING] CONNECTION LIMITE REACHED\n";
                close(conn_id);
            } else {
                cout << "[INFO] NEW CONNECTION ACCEPTED FROM " << inet_ntoa(client.sin_addr)
                     << ":" << ntohs(client.sin_port) << "\n";
                // create new thread for new connection
                if (pthread_create(&thread_id, &attr, this->connection_handler, new int(conn_id)) == -1) {
                    cout << "[WARNING] CAN'T CREATE NEW THREAD\n";
                    close(conn_id);
                } else {
                    cout << "[INFO] NEW THREAD CREATED\n";
                    connection_++; // increase connection count
                }

            }
        }
    }
}

void* Server::connection_handler(void* sock_fd) {
    /* clock_t clock(void) returns the number of clock ticks
       elapsed since the program was launched.To get the number
           of seconds used by the CPU, you will need to divide by
           CLOCKS_PER_SEC.where CLOCKS_PER_SEC is 1000000 on typical
           32 bit system.  */
    clock_t start, end;

    // Recording the starting clock tick.
    start = clock();

    // byte size
    int read_byte = 0;

    // Get the socket descriptor
    int conn_id = *(int*) sock_fd;

    // request data
    char buffer[BUFFER_SIZE] = {0};

    // response data
    char response[] = "Hello";

    // read response continue
    while ((read_byte = recv(conn_id, buffer, BUFFER_SIZE, 0)) > 0) {
        cout << "[RECEIVED] " << buffer << "\n";

        Evenement eve(buffer);

        Data* donnees = eve.qualification();

        if (donnees == nullptr) {
            cout << "[INFO] Donnees recues non conforme" << endl;
        } else {
            sqlite3* db;
            int rc;
            rc = sqlite3_open("../db/deminage.db", &db);
            if (rc != SQLITE_OK) {
                cout << "[BDD] Erreur ouverture base: " << sqlite3_errmsg(db) << endl;
            }
            cout << "[BDD] OK Ouverture de la base" << endl;

            donnees->insert(db);
            sqlite3_close(db);
            delete donnees;
        }

        memset(buffer, 0, BUFFER_SIZE);

        // send response
        if (send(conn_id, response, strlen(response), 0) > 0) {
            cout << "[SEND] " << response << "\n";
        } else {
            cout << "[WARNING][SEND] " << strerror(errno) << "\n";
        }

    }

    // terminate connection
    close(conn_id);
    cout << "[INFO] CONNECTION CLOSED\n";

    // decrease connection counts
    connection_--;

    // thread automatically terminate after exit connection handler
    cout << "[INFO] THREAD TERMINATED" << endl;

    delete (int*) sock_fd;

    // Recording the end clock tick.
    end = clock();

    // Calculating total time taken by the program.
    double time_taken = double(end - start) / double(CLOCKS_PER_SEC);

    // print process time
    cout << "[TIME] PROCESS COMPLETE IN " << fixed << time_taken << setprecision(5);
    cout << " SEC\n";

    // print line
    cout << "------------------------\n";

    // exiting
    pthread_exit(NULL);
}