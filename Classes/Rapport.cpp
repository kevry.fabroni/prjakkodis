/*!
 * \file Rapport.cpp
 * \brief Classe pour la manipulation des rapports de mission
 * \author SeptimusTech
 */

#include "Rapport.h"

using namespace std;

Rapport::Rapport(cJSON *data_json) {
    cJSON *json_handler = nullptr;
    //Expediteur
    json_handler = cJSON_GetObjectItem(data_json, "Expediteur");
    if (json_handler == nullptr) {
        this->expediteur = "inconnu";
    } else {
        this->expediteur = json_handler->valuestring;
    }

    //Id ODM
    json_handler = nullptr;
    json_handler = cJSON_GetObjectItem(data_json, "Id");
    if (json_handler == nullptr) json_handler = cJSON_GetObjectItem(data_json, "ID_ODM");
    if (json_handler != nullptr) {
        this->idODM = json_handler->valuestring;
    } else {
        this->idODM = "inconnu";
    }

    //Info rapport
    json_handler = nullptr;
    json_handler = cJSON_GetObjectItem(data_json, "Message");
    if (json_handler == nullptr) json_handler = cJSON_GetObjectItem(data_json, "Rapport");
    if (json_handler != nullptr) {
        this->infoRapport = json_handler->valuestring;
    } else {
        this->infoRapport = "inconnu";
    }
    this->idRapport = "RDM_" + horodater();
}

Rapport::~Rapport() {}


string Rapport::toJSON() {
    return "";
}

int Rapport::insert(sqlite3 *db) {
    sqlite3_stmt *stmt_ins;
    const char *rq_ins = "insert into rapports (id_rapport, id_odm, description, expediteur) values (?, ?, ?, ?)";
    int rc = sqlite3_prepare_v2(db, rq_ins, strlen(rq_ins), &stmt_ins, nullptr);

    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur préparation rq insert : " << endl;
    }

    sqlite3_bind_text(stmt_ins, 1, idRapport.c_str(), strlen(idRapport.c_str()), nullptr);
    sqlite3_bind_text(stmt_ins, 2, idODM.c_str(), strlen(idODM.c_str()), nullptr);
    sqlite3_bind_text(stmt_ins, 3, infoRapport.c_str(), strlen(infoRapport.c_str()), nullptr);
    sqlite3_bind_text(stmt_ins, 4, expediteur.c_str(), strlen(expediteur.c_str()), nullptr);

    sqlite3_step(stmt_ins);
    return rc;
}