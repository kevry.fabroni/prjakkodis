/*!
 * \file Rapport.h
 * \brief Classe pour la manipulation des rapports de mission
 * \author SeptimusTech
 */

#ifndef PRJAKKODIS_RAPPORT_H
#define PRJAKKODIS_RAPPORT_H

#include <string>
#include <cstring>
#include <stdio.h>
#include <iostream>
#include <sqlite3.h>
#include "Data.h"
#include "cJSON.h"

using namespace std;

/*!
 * \class Rapport Rapport.h Rapport.cpp
 * \brief Classe pour la manipulation des rapports de mission
 */
class Rapport : public Data {

public:
    Rapport();

    virtual ~Rapport();

    Rapport(cJSON*);

    string toJSON() override;

    int insert(sqlite3* db) override;

    void affiche();

private:
    string idRapport;
    string infoRapport;
    string idODM;
    string expediteur;
};


#endif //PRJAKKODIS_RAPPORT_H
