#include "Status.h"
#include <iostream>

using namespace std;

const char *Status::statusTab_[4] = {
        "ECHEC",
        "REUSSIT",
        "ANNULER",
        "ENCOURS"
};

Status::Status() {
    // Rien à faire ici pour le moment
}

const char **Status::getStatusTab() {
    return statusTab_;
}

string Status::findStatus(const std::string &statusToFind) {
    std::string lowercaseStatusToFind;
    for (char c: statusToFind) {
        lowercaseStatusToFind += static_cast<char>(tolower(c));
    }

    for (size_t i = 0; i < sizeof(statusTab_) / sizeof(statusTab_[0]); ++i) {
        std::string currentStatus = statusTab_[i];
        std::string lowercaseCurrentStatus;
        for (char c: currentStatus) {
            lowercaseCurrentStatus += static_cast<char>(tolower(c));
        }

        if (lowercaseCurrentStatus == lowercaseStatusToFind) {
            return statusTab_[i];
        }
    }

    return "Non trouvé";
}

//Accesseur/Mutatteur

string Status::getStatus() {
    return status_;
}

void Status::setStatus(const string status) {
    string foundStatus = findStatus(status); // Recherche du statut dans le tableau
    if (foundStatus != "Non trouvé") {
        status_ = foundStatus; // On met à jour le statut de l'objet avec la bonne casse
    } else {
        //on ignore la mise à jour du statut.
        cout << "Statut non trouvé. Le statut n'a pas été mis à jour." << endl;
    }
}