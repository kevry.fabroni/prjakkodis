/*!
 * \file Data.cpp
 * \brief Classe virtuelle pour la manipulation des données structurées en objets
 * \author SeptimusTech
 */

 #include "Data.h"

Data::~Data() {}

string Data::horodater() {
    time_t now = time(0);
    time(&now);
    char horodate[sizeof "2023-07-25T12:30:30Z"];
    strftime(horodate, sizeof horodate, "%FT%TZ", gmtime(&now));
    return horodate;
}