/*!
 * \file Data.h
 * \brief Classe virtuelle pour la manipulation des données structurées en objets
 * \author SeptimusTech
 */

#ifndef PRJAKKODIS_DATA_H
#define PRJAKKODIS_DATA_H

#include <string>
#include <sqlite3.h>
#include <ctime>

using namespace std;
/*!
 * \class Data Data.h Data.cpp
 * \brief Classe virtuelle pour la manipulation des données structurées en objets
 */
class Data {
public:

    virtual ~Data();

    /*!
     * \fn virtual string toJSON()
     * \brief Fonction de création d'une chaine représentant le JSON de l'objet
     * @return string d'un JSON sans formatage
     */
    virtual string toJSON() = 0;

    /*!
     * \fn void insert(sqlite3* db)
     * \brief Insert l'objet actuel dans la base de données SQLITE db
     * @param db Pointeur sur structure de description d'une base de données SQLITE
     * @return retour erreur
     */
    virtual int insert(sqlite3* db) = 0;

    /*!
     * \fn string horodater()
     * \brief Fonction d'horodatage, utilisée pour la création d'identifiants
     * @return string d'une date et heure sous format ISO-8601, heure UTC
     */
    string horodater();

};


#endif //PRJAKKODIS_DATA_H
