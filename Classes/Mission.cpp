#include <iostream>
#include <cstring>
#include "Mission.h"


using namespace std;

//Mission::Mission(string id, string descriptifMission, string idRapport, string status, list <ODM> listODMs)
//        : idMission_(id), descriptif_(descriptifMission), idRapport_(idRapport), status_(status), listODMs_(listODMs) {
//
//}
//
//Mission::Mission(string id, string descriptifMission, string idRapport, string status)
//        : idMission_(id), descriptif_(descriptifMission), idRapport_(idRapport), status_(status) {
//
//}
//
//Mission::Mission(string id, string descriptifMission, string idRapport)
//        : idMission_(id), descriptif_(descriptifMission), idRapport_(idRapport) {
//
//}
//
Mission::Mission(string id, string descriptifMission)
        : idMission_(id), descriptif_(descriptifMission) {

}

Mission::Mission(string descriptifMission) {
    setDescriptif(descriptifMission);
    setIdMission(code_ + "_" + horodater());
}

//Mission::Mission(string id)
//        : idMission_(id) {
//}

Mission::Mission() {
    idMission_ = code_ + "_" + horodater();
}

Mission::~Mission() {

}

string Mission::toJSON() {
    return string();
}

//void Mission::ajouterODM(string &idODM) {
//    ODM nouvelleODM;
//    nouvelleODM.setIdODM(idODM);
//
//    listODMs_.push_back(nouvelleODM);
//}

const ODM &Mission::getLastODM() const {
    if (listODMs_.empty()) {
        std::out_of_range("La liste des ODM est vide");
    }
    return listODMs_.back(); // Retourne la dernière ODM en date (le dernier élément de la liste)
}

const list <ODM> &Mission::getAllODMs() const {
    return listODMs_; // Retourne la liste complète des ODMs
}


//string Mission::serializeODMs() {
//    string serializedODMs;
//    for (auto it = listODMs_.begin(); it != listODMs_.end(); ++it) {
//        if (it != listODMs_.begin()) {
//            serializedODMs += ",";
//        }
//        serializedODMs += it->getIdODM(); // Concatène l'ID de chaque ODM, séparé par une virgule
//    }
//    return serializedODMs;
//}

//void Mission::deserializeODMs(string &serializedODMs) {
//    listODMs_.clear(); // Vide la liste actuelle des ODMs avant de désérialiser
//
//    string idODM;
//    size_t pos = 0;
//    while ((pos = serializedODMs.find(',')) != string::npos) {
//        idODM = serializedODMs.substr(0, pos); // Extrait l'ID de l'ODM
//        ODM nouvelleODM;
//        nouvelleODM.setIdODM(idODM); // Définir l'ID de la nouvelle ODM
//
//        listODMs_.push_back(nouvelleODM); // Ajoutez la nouvelle ODM à la fin de la liste
//        serializedODMs.erase(0, pos); // Supprime l'ID sérialisé traité de la chaîne, pas besoin de "+1"
//    }
//
//    if (!serializedODMs.empty()) {
//        ODM nouvelleODM;
//        nouvelleODM.setIdODM(serializedODMs); // Définir l'ID de la dernière nouvelle ODM
//
//        listODMs_.push_back(nouvelleODM); // Ajoutez la dernière nouvelle ODM à la fin de la liste
//    }
//}


string Mission::getIdMission() {
    return idMission_;
}

void Mission::setIdMission(string IdMission) {
    idMission_ = IdMission;
}

string Mission::getDescriptif() {
    return descriptif_;
}

void Mission::setDescriptif(string descriptif) {
    descriptif_ = descriptif;
}


string Mission::getIdRapport() {
    return idRapport_;
}

void Mission::setIdRapport(string idRapport) {
    idRapport_ = idRapport;
}


string Mission::getStatus() {
    return status_;
}

void Mission::setStatus(string status) {
    status_ = status;
}

// Méthodes CRUD
int Mission::insert(sqlite3 *db) {
    sqlite3_stmt *stmt_ins;
    const char *rq_ins = "INSERT INTO mission (id_mission, descriptif, id_rapport, ODMs, status) VALUES (?, ?, ?, ?, ?)";
    int rc = sqlite3_prepare_v2(db, rq_ins, strlen(rq_ins), &stmt_ins, nullptr);

    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur préparation requête insert : " << sqlite3_errmsg(db) << endl;
        return rc;
    }

    // Binding des paramètres
    sqlite3_bind_text(stmt_ins, 1, idMission_.c_str(), strlen(idMission_.c_str()), nullptr);
    sqlite3_bind_text(stmt_ins, 2, descriptif_.c_str(), strlen(descriptif_.c_str()), nullptr);
    sqlite3_bind_text(stmt_ins, 3, idRapport_.c_str(), strlen(idRapport_.c_str()), nullptr);

    // Serialize the ODMs and bind it as text
//    string serializedODMs = serializeODMs();
//    sqlite3_bind_text(stmt_ins, 4, serializedODMs.c_str(), serializedODMs.length(), nullptr);
    sqlite3_bind_text(stmt_ins, 4, "", 0, nullptr);

    sqlite3_bind_text(stmt_ins, 5, status_.c_str(), strlen(status_.c_str()), nullptr);

    // Exécution de la requête
    rc = sqlite3_step(stmt_ins);

    // Libération des ressources
    sqlite3_finalize(stmt_ins);

    return rc;
}

// Méthode pour sélectionner une mission à partir de son ID dans la base de données
//int Mission::select(sqlite3 *db, string id, Mission *instance) {
//    sqlite3_stmt *stmt_select;
//    const char *rq_select = "SELECT id_mission, descriptif, id_rapport, ODMs, status FROM mission WHERE idMission = ?";
//    int rc = sqlite3_prepare_v2(db, rq_select, strlen(rq_select), &stmt_select, nullptr);
//
//    if (rc != SQLITE_OK) {
//        cout << "[BDD] Erreur préparation requête select : " << sqlite3_errmsg(db) << endl;
//        return rc;
//    }
//
//    // Binding des paramètres
//    sqlite3_bind_text(stmt_select, 1, id.c_str(), strlen(id.c_str()), nullptr);
//
//    // Exécution de la requête
//    rc = sqlite3_step(stmt_select);
//
//    if (rc == SQLITE_ROW) {
//        // Récupération des valeurs
//        instance->setIdMission(reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 0)));
//        instance->setDescriptif(reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 1)));
//        instance->setIdRapport(reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 2)));
//        instance->setStatus(reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 4)));
//
//        // Deserialize ODMs and populate the list
//        string serializedODMs(reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 3)));
//        instance->deserializeODMs(serializedODMs);
//    } else if (rc != SQLITE_DONE) {
//        cout << "[BDD] Erreur lors de l'exécution de la requête select : " << sqlite3_errmsg(db) << endl;
//    }
//
//    // Libération des ressources
//    sqlite3_finalize(stmt_select);
//
//    return rc;
//}

// Méthode pour rechercher une mission par son ID dans la base de données
//Mission *Mission::findById(sqlite3 *db, const string id) {
//    Mission *instance = new Mission();
//    int rc = instance->select(db, id, instance);
//    if (rc == SQLITE_ROW) {
//        return instance;
//    } else {
//        delete instance;
//        return nullptr;
//    }
//}


void Mission::afficher() {
    cout << "Mission : " << getIdMission() << endl;
    cout << "          " << getDescriptif() << endl;
    cout << endl;
}