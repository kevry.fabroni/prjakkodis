#ifndef PRJAKKODIS_MISSION_H
#define PRJAKKODIS_MISSION_H

#include <string>
#include <sqlite3.h>
#include <list>
#include "Data.h"
#include "cJSON.h"
#include "Status.h"
#include "ODM.h"

using namespace std;

class Mission : public Data {
public:
//    Mission(string id, string descriptifMission, string idRapport, string status, list <ODM> listODMs_);
//
//    Mission(string id, string descriptifMission, string idRapport, string status);
//
//    Mission(string id, string descriptifMission, string idRapport);

    Mission(string id, string descriptifMission);

    Mission(string descriptifMission);

    Mission();

    virtual ~ Mission();

    string toJSON() override;//Pas implémenter pour Mission

    // Serialisation et désérialisation des ODMs
//    string serializeODMs();
//
//    void deserializeODMs(string &serializedODMs);

    //CRUD
    int insert(sqlite3 *db) override;

    int select(sqlite3 *db, string id, Mission *instance);

//    static Mission *findById(sqlite3 *db, const string id);


    //Accesseur/Mutaters
    string getIdMission();

    void setIdMission(string IdMission);


    string getDescriptif();

    void setDescriptif(string descriptif);

    string getIdRapport();

    void setIdRapport(string idRapport);

    string getStatus();

    void setStatus(string status);


//    void ajouterODM(string &idODM);

    const ODM &getLastODM() const;

    const list <ODM> &getAllODMs() const;

    //Affichage
    void afficher();


private:
    string const code_ = "MISSION";
    string idMission_;
    string descriptif_;
    string idRapport_;
    string lastODM;
    list <ODM> listODMs_;
    string status_;


};


#endif //PRJAKKODIS_MISSION_H
