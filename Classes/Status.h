#ifndef PRJAKKODIS_STATUS_H
#define PRJAKKODIS_STATUS_H

#include <string>

using namespace std;

class Status {
public:
    Status();

    //Accesseur
    static const char **getStatusTab();

    static std::string findStatus(const std::string &statusToFind);

    string getStatus();

    void setStatus(const string status);


private:
    static const char *statusTab_[4];
    string status_;
};


#endif //PRJAKKODIS_STATUS_H
