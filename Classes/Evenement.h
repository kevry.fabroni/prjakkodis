/*!
 * \file Evenement.h
 * \brief Classe pour la manipulation des données entrante via le serveur
 * \author SeptimusTech
 */
#ifndef PRJAKKODIS_EVENEMENT_H
#define PRJAKKODIS_EVENEMENT_H

#include <iostream>
#include <cstring>
#include <string>
#include "Data.h"
#include "ODM.h"
#include "Rapport.h"
#include "cJSON.h"

using namespace std;

/*!
 * \class Evenement Evenement.h Evenement.cpp
 * \brief Evenement Classe pour la manipulation des données entrante via le serveur
 */
class Evenement {
public:
    /*!
     * Constructeur : affectation de @data à l'attribut @donnees_recues
     * @param data String de donnée entrant sur le serveur
     */
    Evenement(string data);

    /*!
     * Qualifie la données entrante en vérifiant le format et le code d'entrée
     * @return Une instance de l'objet représenté par donnees_recues ou nullptr
     */
    Data* qualification();


private:
    //! @param donnees_recues String brut des données reçues du serveur
    string donnees_recues;
};


#endif //PRJAKKODIS_EVENEMENT_H
