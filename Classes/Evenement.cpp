/*!
 * \file Evenement.cpp
 * \brief Classe pour la manipulation des données entrante via le serveur
 * \author SeptimusTech
 */

#include "Evenement.h"

Evenement::Evenement(string data) {
    donnees_recues = data;
}

Data *Evenement::qualification() {
    Data *data;
    data = nullptr;

    cJSON *data_json = cJSON_Parse(donnees_recues.c_str());
    if (data_json == NULL) {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr == NULL) {
            cout << "[QUALIF] : parse JSON : erreur avant " << error_ptr << endl;
        }
        cout << "[QUALIF] : c'est pas du JSON" << endl;
    } else {
        cJSON *json_code_entree = cJSON_GetObjectItem(data_json, "Code");
        if (json_code_entree == nullptr) {
            cout << "[QUALIF] pas de code" << endl;
        } else {
/*            if (strcmp(json_code_entree->valuestring, "ODM") == 0) {
                data = new ODM("test");
            } else */
            if (strcmp(json_code_entree->valuestring, "RDM") == 0) {
                data = new Rapport(data_json);
            } else {
                cout << "[QUALIF] code inconnu" << endl;
            }
        }
    }
    return data;
}