/*!
 * \file ODM.h
 * \brief Classe pour la manipulation des ordres de mission
 * \author SeptimusTech
 */

#ifndef PRJAKKODIS_ODM_H
#define PRJAKKODIS_ODM_H

#include <string>
#include <sqlite3.h>
#include "Data.h"
#include "cJSON.h"

using namespace std;

/*!
 * \class ODM ODM.h ODM.cpp
 * \brief Classe pour la manipulation des ordres de mission
 */
class ODM : public Data {
public:
    ODM(string id, string instructions);

    ODM(string instructions);

    ODM();

    virtual ~ODM();

    string toJSON() override;

    //CRUD
    int insert(sqlite3* db) override;

    /*!
     * \fn int select(sqlite3* db, string id, ODM* instance)
     * \brief Charge depuis la bdd les données ODM depuis son id
     * @param db manipulateur de la bdd
     * @param id id_odm pour retrouver
     * @param instance ODM à renseigner
     * @return
     */
    int select(sqlite3* db, string id, ODM* instance);


    //Accesseur/Mutaters
    string const getCodeODM();

    string getSenderODM();

    void setSender(string sender);

    string getInstruction();

    void setInstruction(string instruction);

    string getIdRapport();

    void setIdRapport(string idRapport);

    //Affichage
    void afficher();


private:
    string const code_ = "ODM";
    string const sender_ = "GR7";
    string idODM_;
    string instructions_;
    string idRapport_;
};


#endif //PRJAKKODIS_ODM_H
