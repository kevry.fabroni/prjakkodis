/*!
 * \file ODM.cpp
 * \brief Classe pour la manipulation des ordres de mission
 * \author SeptimusTech
 */

#include <iostream>
#include <cstring>
#include "ODM.h"

using namespace std;

ODM::ODM(string id, string instructions) {
    idODM_ = id;
    instructions_ = instructions;
}

ODM::ODM(string instructions) {
    idODM_ = code_ + "_" + horodater();
    instructions_ = instructions;
}


ODM::ODM() {
    idODM_ = code_ + "_" + horodater();
}

// Implémentation du destructeur
ODM::~ODM() {
    // Rien à faire ici, mais on pourrai ajouter du code de nettoyage si nécessaire
}

string ODM::toJSON() {
    cJSON *root;
    root = cJSON_CreateObject();

    cJSON_AddItemToObject(root, "Code", cJSON_CreateString(code_.c_str()));
    cJSON_AddItemToObject(root, "Expediteur", cJSON_CreateString(sender_.c_str()));
    cJSON_AddItemToObject(root, "ID_ODM", cJSON_CreateString(idODM_.c_str()));
    cJSON_AddItemToObject(root, "Instructions", cJSON_CreateString(instructions_.c_str()));

    return cJSON_PrintUnformatted(root);
}

//Getter Setter

string const ODM::getCodeODM() {
    return code_;
}

string ODM::getSenderODM() {
    return sender_;
}

string ODM::getInstruction() {
    return instructions_;
}

void ODM::setInstruction(string instruction) {
    // On peut ajouter des validations supplémentaires ici si nécessaire
    instructions_ = instruction;
}

string ODM::getIdRapport() {
    return idRapport_;
}

void ODM::setIdRapport(string idRapport) {
    // On peut ajouter des validations supplémentaires ici si nécessaire
    idRapport_ = idRapport;
}

int ODM::insert(sqlite3 *db) {
    sqlite3_stmt *stmt_ins;
    const char *rq_ins = "insert into odm(id_odm, instructions) values(?,?)";
    int rc = sqlite3_prepare_v2(db, rq_ins, strlen(rq_ins), &stmt_ins, nullptr);

    if (rc != SQLITE_OK) {
        cout << "[BDD] Erreur préparation rq insert : " << endl;
    }

    sqlite3_bind_text(stmt_ins, 1, idODM_.c_str(), strlen(idODM_.c_str()), nullptr);
    sqlite3_bind_text(stmt_ins, 2, instructions_.c_str(), strlen(instructions_.c_str()), nullptr);

    sqlite3_step(stmt_ins);
    return rc;
}

int ODM::select(sqlite3 *db, string id, ODM *instance) {
    sqlite3_stmt *stmt_select;
    const char *rq_select = "SELECT * FROM odm WHERE id_odm = ?";
    int rc = sqlite3_prepare_v2(db, rq_select, strlen(rq_select), &stmt_select, nullptr);

    if (rc != SQLITE_OK) {
        std::cerr << "[BDD] Erreur préparation requête select : " << sqlite3_errmsg(db) << std::endl;
        return rc;
    }

    // Binding des paramètres
    sqlite3_bind_text(stmt_select, 1, id.c_str(), strlen(id.c_str()), nullptr);

    // Exécution de la requête
    rc = sqlite3_step(stmt_select);

    if (rc == SQLITE_ROW) {
        // Récupération des valeurs
        instance->idODM_ = reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 0));
        instance->instructions_ = reinterpret_cast<const char *>(sqlite3_column_text(stmt_select, 1));
    } else if (rc != SQLITE_DONE) {
        std::cerr << "[BDD] Erreur lors de l'exécution de la requête select : " << sqlite3_errmsg(db) << std::endl;
    }

    // Libération des ressources
    sqlite3_finalize(stmt_select);

    return rc;
}


void ODM::afficher() {
    cout << "ODM : " << idODM_ << endl;
    cout << "      " << instructions_ << endl;
    cout << endl;
}